package com.zero2oneit.mall.member.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zero2oneit.mall.common.bean.member.PrizeWin;
import com.zero2oneit.mall.common.query.member.PrizeWinQueryObject;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zero2oneit.mall.member.mapper.PrizeWinMapper;
import com.zero2oneit.mall.member.service.PrizeWinService;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-25
 */
@Service
public class PrizeWinServiceImpl extends ServiceImpl<PrizeWinMapper, PrizeWin> implements PrizeWinService {

    @Autowired
    private PrizeWinMapper prizeWinMapper;

    @Override
    public BoostrapDataGrid pageList(PrizeWinQueryObject qo) {
        //查询总记录数
        int total = prizeWinMapper.selectTotal(qo);
        return new BoostrapDataGrid(total, total == 0 ? Collections.EMPTY_LIST : prizeWinMapper.selectRows(qo));
    }

    @Override
    public List<HashMap<String, Object>> selectList(String memberId) {
        return prizeWinMapper.selectLists(memberId);
    }

}