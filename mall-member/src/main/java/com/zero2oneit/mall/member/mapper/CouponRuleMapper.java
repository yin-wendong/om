package com.zero2oneit.mall.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zero2oneit.mall.common.bean.member.CouponRule;
import com.zero2oneit.mall.common.query.member.CouponRuleQueryObject;
import com.zero2oneit.mall.common.query.member.MemberCouponQueryObject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author Tg
 * @create 2021-03-19
 * @description
 */
@Mapper
public interface CouponRuleMapper extends BaseMapper<CouponRule> {

    int selectTotal(CouponRuleQueryObject qo);

    List<Map<String, Object>> selectAll(CouponRuleQueryObject qo);

    void status(@Param("id") String id, @Param("status") Integer status);

    void reduce(MemberCouponQueryObject qo);

    void copyRule(String id);

}
