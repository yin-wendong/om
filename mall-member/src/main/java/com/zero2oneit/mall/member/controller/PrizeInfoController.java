package com.zero2oneit.mall.member.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zero2oneit.mall.common.bean.member.PrizeInfo;
import com.zero2oneit.mall.common.query.member.PrizeInfoQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import com.zero2oneit.mall.member.service.PrizeInfoService;

import java.util.List;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-24
 */
@RestController
@RequestMapping("/admin/member/prizeInfo")
public class PrizeInfoController {

    @Autowired
    private PrizeInfoService prizeInfoService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    /**
     * 查询中奖奖品列表信息
     * @param qo
     * @return
     */
    @PostMapping("/list")
    public BoostrapDataGrid list(@RequestBody PrizeInfoQueryObject qo){
        return prizeInfoService.pageList(qo);
    }

    /**
     * 添加或编辑中奖奖品信息
     * @param prizeInfo
     * @return
     */
    @PostMapping("/addOrEdit")
    public R addOrEdit(@RequestBody PrizeInfo prizeInfo){

        prizeInfoService.saveOrUpdate(prizeInfo);

        //根据类型查询设置到
        QueryWrapper<PrizeInfo> wrapper = new QueryWrapper();
        List<PrizeInfo> prizeInfos = prizeInfoService.list(wrapper);
        redisTemplate.opsForValue().set("om:prizeInfo:list", JSON.toJSONString(prizeInfos));

        return R.ok();
    }

}
